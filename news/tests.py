import datetime
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import News


def create_news(spoiler, days, content='Dummy'):
    """
    Creates a content with the given `spoiler`, `content` and published the
    given number of `days` offset to now (negative for news published
    in the past, positive for news that have yet to be published).
    """
    time = timezone.now() + datetime.timedelta(days=days)
    return News.objects.create(spoiler=spoiler, content=content, pub_date=time)


class NewsMethodTests(TestCase):

    def test_was_published_recently_with_future_news(self):
        """
        was_published_recently() should return False for news whose
        pub_date is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_news = News(pub_date=time)
        self.assertIs(future_news.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() should return False for questions whose
        pub_date is older than 1 day.
        """
        time = timezone.now() - datetime.timedelta(days=30)
        old_question = News(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        """
        was_published_recently() should return True for questions whose
        pub_date is within the last day.
        """
        time = timezone.now() - datetime.timedelta(hours=1)
        recent_question = News(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)


class NewsViewTests(TestCase):
    def test_index_view_with_no_news(self):
        """
        If no news exist, an appropriate message should be displayed.
        """
        response = self.client.get(reverse('news:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Новостей пока что нет')
        self.assertQuerysetEqual(response.context['latest_news'], [])

    def test_index_view_with_a_past_news(self):
        """
        News with a pub_date in the past should be displayed on the
        index page.
        """
        create_news(spoiler="Past news.", days=-30)
        response = self.client.get(reverse('news:index'))
        self.assertQuerysetEqual(
            response.context['latest_news'],
            ['<News: Past news.>']
        )

    def test_index_view_with_a_future_question(self):
        """
        News with a pub_date in the future should not be displayed on
        the index page.
        """
        create_news(spoiler="Future news.", days=30)
        response = self.client.get(reverse('news:index'))
        self.assertContains(response, "Новостей пока что нет")
        self.assertQuerysetEqual(response.context['latest_news'], [])

    def test_index_view_with_future_question_and_past_question(self):
        """
        Even if both past and future questions exist, only past questions
        should be displayed.
        """
        create_news(spoiler="Past news.", days=-30)
        create_news(spoiler="Future news.", days=30)
        response = self.client.get(reverse('news:index'))
        self.assertQuerysetEqual(
            response.context['latest_news'],
            ['<News: Past news.>']
        )

    def test_index_view_with_two_past_questions(self):
        """
        The news index page may display multiple news.
        """
        create_news(spoiler="Past news 1.", days=-30)
        create_news(spoiler="Past news 2.", days=-5)
        response = self.client.get(reverse('news:index'))
        self.assertQuerysetEqual(
            response.context['latest_news'],
            ['<News: Past news 2.>', '<News: Past news 1.>']
        )


class NewsIndexDetailTests(TestCase):
    def test_detail_view_with_a_future_news(self):
        """
        The detail view of a news with a pub_date in the future should
        return a 404 not found.
        """
        future_news = create_news(spoiler='Future news.', days=5)
        url = reverse('news:detail', args=(future_news.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_detail_view_with_a_past_question(self):
        """
        The detail view of a question with a pub_date in the past should
        display the question's text.
        """
        past_news = create_news(spoiler='Past news.', days=-5)
        url = reverse('news:detail', args=(past_news.id,))
        response = self.client.get(url)
        self.assertContains(response, past_news.spoiler)
        self.assertContains(response, past_news.content)
