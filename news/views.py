from django.views import generic
from django.utils import timezone

from .models import News


class IndexView(generic.ListView):
    template_name = 'news/index.html'
    context_object_name = 'latest_news'

    def get_queryset(self):
        return News.objects.filter(pub_date__lte=timezone.now())\
            .order_by('-pub_date')


class DetailView(generic.DetailView):
    model = News
    template_name = 'news/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return News.objects.filter(pub_date__lte=timezone.now())
