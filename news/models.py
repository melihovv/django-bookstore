import datetime
from django.db import models
from django.utils import timezone
from django.utils.translation import pgettext


class News(models.Model):
    spoiler = models.CharField(max_length=255)
    content = models.TextField()
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.spoiler

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    class Meta:
        verbose_name = pgettext("news singular", "news")
        verbose_name_plural = pgettext("news plural", "news")
