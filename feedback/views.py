from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import FeedbackForm


def index(request):
    if request.method == 'POST':
        form = FeedbackForm(data=request.POST)

        if form.is_valid():
            form.send_email()
            messages.info(request, 'Ваше сообщение успешно отправлено')
            return HttpResponseRedirect(reverse('home:index'))
    else:
        form = FeedbackForm()

    return render(request, 'feedback/index.html', {'form': form})
