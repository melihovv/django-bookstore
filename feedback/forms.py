from django import forms
from captcha.fields import CaptchaField
from django.core.mail import send_mail
from django.template.loader import render_to_string

from bookstore.private_settings import EMAIL_HOST_USER, EMAIL_TO


class FeedbackForm(forms.Form):
    email = forms.EmailField(label='Email', required=True)
    message = forms.CharField(widget=forms.Textarea, label='Ваше сообщение',
                              required=True)
    captcha = CaptchaField(label='Введите капчу', required=True)

    def send_email(self):
        context = {
            'email': self.cleaned_data['email'],
            'message': self.cleaned_data['message'],
        }
        send_mail(
            'Сообщение с формы обратной связи',
            render_to_string('feedback/email.txt', context),
            EMAIL_HOST_USER,
            [EMAIL_TO],
            fail_silently=False,
            html_message=render_to_string('feedback/email.html', context)
        )
