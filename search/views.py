from django.contrib import messages
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from books.models import Book
from news.models import News


def search_books(query, results):
    search_results = Book.objects.filter(
        Q(name__icontains=query)
        | Q(description__icontains=query)
        | Q(author__icontains=query)
    )
    for result in search_results:
        results.append({
            'url': reverse('books:detail', args=[result.id]),
            'header': 'Книга: {}'.format(result.name),
        })


def search_news(query, results):
    search_results = News.objects.filter(
        Q(spoiler__icontains=query) | Q(content__icontains=query)
    )
    for result in search_results:
        results.append({
            'url': reverse('news:detail', args=[result.id]),
            'header': 'Новость: {}'.format(result.spoiler),
        })


def search(request):
    query = request.POST['query']

    if not query:
        messages.error(request, 'Вы указали пустое значение для поиска')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    results = []
    search_books(query, results)
    search_news(query, results)

    return render(request, 'search/results.html', {
        'query': query,
        'results': results,
    })
