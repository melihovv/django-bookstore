import os
from django.db import models


def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)


class Book(models.Model):
    name = models.CharField(max_length=255, unique=True)
    author = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    amount = models.PositiveIntegerField()
    photo = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    price = models.PositiveIntegerField()

    def __str__(self):
        return self.name
