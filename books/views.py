from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic

from .models import Book
from .forms import BookOrderForm


class IndexView(generic.ListView):
    template_name = 'books/index.html'
    context_object_name = 'books'

    def get_queryset(self):
        return Book.objects.filter(amount__gt=0)


def detail(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    form = BookOrderForm()

    if request.method == 'POST':
        form = BookOrderForm(data=request.POST)

        if form.is_valid():
            form.send_email(book)
            messages.info(request, 'Ваш заказ успешно принят')
            return HttpResponseRedirect(reverse('books:index'))

    return render(request, 'books/detail.html', {'form': form, 'book': book})
