from django import forms
from captcha.fields import CaptchaField
from django.core.mail import send_mail
from django.template.loader import render_to_string

from bookstore.private_settings import EMAIL_TO, EMAIL_HOST_USER


class BookOrderForm(forms.Form):
    email = forms.EmailField(label='Email', required=True)
    amount = forms.IntegerField(label='Количество', required=True)
    captcha = CaptchaField(label='Введите капчу', required=True)

    def send_email(self, book):
        context = {
            'book': book,
            'amount': self.cleaned_data['amount'],
            'email': self.cleaned_data['email'],
        }
        send_mail(
            'Заказ книги',
            render_to_string('books/email.txt', context),
            EMAIL_HOST_USER,
            [EMAIL_TO],
            fail_silently=False,
            html_message=render_to_string('books/email.html', context)
        )
