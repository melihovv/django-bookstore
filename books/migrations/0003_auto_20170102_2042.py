# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-02 17:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0002_auto_20170102_2040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='amount',
            field=models.PositiveIntegerField(),
        ),
    ]
